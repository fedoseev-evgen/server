const Ad = require('./../../db/Schma');

module.exports=function (req, res){
    // Админ вводит в форму пароль и иденификатор записи
    // при переходе на эту страницу, если пароль === '123', запись получает typeCorrect = true
    Ad.findById({ _id: req.body.id }, (err, result) => {
        if (err||result===null)
            res.send(`Запись с id ${req.body.id} не найдена!`);
        else if (req.body.password === 'logan') {
            if (result.typeCorrect === false)
            {
                result.typeCorrect = true;
                result.save((err) => {
                    if (err) {  // вывод сообщения об ошибке
                        res.status(200).send(err);
                    }
                    else {
                        res.send(`Запись ${result.title} успешно одобрена`)
                    }
                });
            }
            else res.send(`Запись ${result.title} уже одобрена!`)    
        }
        else
           res.send(`Неверный пароль!`);
     });
}